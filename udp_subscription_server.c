#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>

#include "udp_subscription_server.h"
#include "err.h"
#include "timeutils.h"

void lockList(client_list_t* list) {
    if (pthread_mutex_lock(&list->mutex) < 0) {
        syserr("Mutex lock");
    }
}

void unlockList(client_list_t* list) {
    if (pthread_mutex_unlock(&list->mutex) < 0) {
        syserr("Mutex unlock");
    }
}

bool refreshClient(client_list_t* list, client_t* client, char* desc) {
    client_list_node_t* node = list->head;

    while (node != NULL) {
        if (isSameClient(client, node->peer)) {
            // this client was on the list, update time and end
            if (node->description != NULL) free(node->description);
            if (desc != NULL) node->description = desc;


            setCurrentTimestamp(&node->time);
            break;
        }

        node = node->next_client;
    }

    /* No client found, this is a new one */
    if (node == NULL) {
        client_t* clientCopy = copyClient(client);
        if (clientCopy == NULL) {
            return false;
        }

        client_list_node_t* newNode = malloc(sizeof(client_list_node_t));
        if (newNode == NULL) {
            freeClient(clientCopy);
            return false;
        }

        newNode->description = desc;
        newNode->next_client = NULL;
        newNode->peer = clientCopy;
        setCurrentTimestamp(&newNode->time);

        if (list->tail == NULL) {
            list->tail = list->head = newNode;
        } else {
            list->tail->next_client = newNode;
            list->tail = newNode;
        }
    }

    return true;
}

void updateAboutMe(client_list_t* list, udp_message_t* message) {
    list->aboutMe = message;
}

void timeoutClients(client_list_t* list) {
    client_list_node_t* prev = NULL;
    client_list_node_t* client = list->head;

    while (client != NULL) {
        /* This client is to be removed */
        if (isTimedOut(&client->time, list->timeout)) {
            /* Remove from tail */
            if (client == list->tail) {
                list->tail = prev;
            }

            /* Was the first - remove from head */
            if (prev == NULL) {
                list->head = client->next_client;
            }

            /* Change list link */
            else {
                prev->next_client = client->next_client;
            }

            client_list_node_t* next = client->next_client;

            freeClient(client->peer);
            free(client);

            client = next;
        }
        else {
            prev = client;
            client = client->next_client;
        }
    }
}

void broadcastMessage(udp_message_t* message, client_list_t* list) {

    timeoutClients(list);

    client_list_node_t* client = list->head;

    while (client != NULL) {
        sendUdpMessage(list->sock, message, client->peer);
        client = client->next_client;
    }
}

void freeClientListNode(client_list_node_t* node) {
    if (node->description != NULL) {
        free(node->description);
    }

    freeClient(node->peer);
    free(node);
}

void cleanupClientList(client_list_t* list) {
    if (list->aboutMe != NULL) {
        freeMessage(list->aboutMe);
    }

    client_list_node_t* node = list->head;
    while (node != NULL) {
        client_list_node_t* toFree = node;
        node = node->next_client;
        freeClientListNode(toFree);
    }
}

