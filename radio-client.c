#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <errno.h>

#include "err.h"
#include "udp_messanger.h"
#include "strutils.h"
#include "udp_subscription_server.h"

#define CLEAR_SCREEN       "\033[2J\033[H"
#define USE_CHARACTER_MODE "\377\375\042\377\373\001"
#define UP_ARROW           "\e[A"
#define DOWN_ARROW         "\e[B"
#define ENTER              "\r"
#define NO_METADATA        "No metadata\0"

client_t* currentClient;
client_t broadcastClient;

pthread_t telnet_thread;
client_list_t radios;

int selectedLine = 0;
int maxLine = 0;

char currentMeta[5000];

int main_sock, telnet_sock;

bool finish;

char* getCurrentInterface() {
    char* buff = malloc(sizeof(char) * 32);
    size_t len = 32;
    size_t end = 0;

    if (buff == NULL)
        syserr("malloc");

    int lineNo = 0;

    append_sprintf(&buff, &len, &end, CLEAR_SCREEN);
    append_sprintf(&buff, &len, &end, "Szukaj posrednika %c\r\n", selectedLine == lineNo ? '*' : ' ');
    lineNo++;

    lockList(&radios);

    client_list_node_t* radio = radios.head;
    while (radio != NULL) {
        append_sprintf(&buff, &len, &end, "%s %c\r\n", radio->description, selectedLine == lineNo ? '*' : ' ');
        lineNo++;
        radio = radio->next_client;
    }

    unlockList(&radios);

    append_sprintf(&buff, &len, &end, "Koniec %c\r\n", selectedLine == lineNo ? '*' : ' ');
    maxLine = lineNo;

    append_sprintf(&buff, &len, &end, "%s\r\n", currentMeta);

    return buff;
}

void selectRadio(int index) {
    lockList(&radios);
    client_list_node_t* node = radios.head;
    for (int i = 0; i < index; i++) node = node->next_client;

    currentClient = node->peer;

    udp_message_t* msg = createMessage(MSG_TYPE_DISCOVER, NULL, 0);
    sendUdpMessage(main_sock, msg, currentClient);
    freeMessage(msg);

    unlockList(&radios);
}

void searchNewRadios() {
    udp_message_t* msg = createMessage(MSG_TYPE_DISCOVER, NULL, 0);
    sendUdpMessage(main_sock, msg, &broadcastClient);
    freeMessage(msg);
}

void endProgram() {
    finish = true;
}

void wrong_format(char* filename) {
    error(true, "Usage: %s -p telnetPort -P port -H host [-T timeout]", filename);
}

void* telnet_interface(void* arg) {

    int* sock = (int*) arg;

    const int telnetBuffSize = 1024;
    char buff[telnetBuffSize];
    struct sockaddr_in client_address;
    socklen_t client_address_len;

    for (;;) {
        client_address_len = sizeof(client_address);

        // get client connection from the socket
        int msg_sock = accept(*sock, (struct sockaddr *) &client_address, &client_address_len);
        if (msg_sock < 0) {
            syserr("accept");
        }

        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 100000;
        if (setsockopt(msg_sock, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
            syserr("set timeout");
        }

        write(msg_sock, USE_CHARACTER_MODE, 6);

        for(;;) {
            char* next = getCurrentInterface();
            write(msg_sock, next, strlen(next));
            free(next);

            ssize_t len = read(msg_sock, &buff, telnetBuffSize);

            if (len < 0) {
                if (errno != EWOULDBLOCK && errno != EAGAIN) {
                    syserr("reading from client socket");
                }
            }
            else {
                buff[len] = '\0';

                if (is_same(buff, UP_ARROW)) {
                    if (selectedLine > 0) selectedLine--;
                }

                if (is_same(buff, DOWN_ARROW)) {
                    if (selectedLine < maxLine) selectedLine++;
                }

                if (is_same(buff, ENTER)) {
                    if (selectedLine == 0) searchNewRadios();
                    else if (selectedLine == maxLine) endProgram();
                    else selectRadio(selectedLine - 1);
                }
            }

            if (finish)
                break;

            if (len == 0)
                break;
        }

        if (close(msg_sock) < 0) {
            syserr("close");
        }

        if (finish)
            break;
    }

    close(telnet_sock);

    return NULL;
}

int createMainSock(short port, char* address, client_t* client) {
    client->addrLen = sizeof(client->addr);

    int sock, optval;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
        syserr("socket");

    optval = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void*)&optval, sizeof optval) < 0)
        syserr("setsockopt broadcast");

    client->addr.sin_family = AF_INET;
    client->addr.sin_port = htons(port);
    if (inet_aton(address, &client->addr.sin_addr) == 0) {
        fprintf(stderr, "ERROR: inet_aton - invalid multicast address\n");
        exit(EXIT_FAILURE);
    }

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
        syserr("set timeout");
    }

    return sock;
}

int createTelnetSock(short telnetPort) {
    int sock;
    struct sockaddr_in server_address;

    sock = socket(PF_INET, SOCK_STREAM, 0);
    if (sock < 0)
        syserr("socket");

    server_address.sin_family = AF_INET; // IPv4
    server_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_address.sin_port = htons(telnetPort);

    // bind the socket to a concrete address
    if (bind(sock, (struct sockaddr *) &server_address, sizeof(server_address)) < 0)
        syserr("bind");

    // switch to listening (passive open)
    if (listen(sock, 1) < 0)
        syserr("listen");

    return sock;
}

void spawnTelnetThread(int* sockptr) {
    if (pthread_create(&telnet_thread, NULL, &telnet_interface, sockptr) < 0) {
        syserr("pthread_create");
    }
}

int main (int argc, char *argv[]) {

    memcpy(currentMeta, NO_METADATA, strlen(NO_METADATA));

    if (pthread_mutex_init(&radios.mutex, NULL) < 0) {
        syserr("mutex_init");
    }

    char* host_str = NULL;
    int port = -1;
    int telnetPort = -1;
    int timeout = 5;

    int opt;
    while ((opt = getopt(argc, argv, "p:P:H:T:")) != -1) {
        switch (opt) {
            case 'p':
                telnetPort = atoi(optarg);
                break;

            case 'P':
                port = atoi(optarg);
                break;

            case 'H':
                host_str = optarg;
                break;

            case 'T':
                timeout = atoi(optarg);
                break;

            default: /* '?' */
                wrong_format(argv[0]);
        }
    }

    if (host_str == NULL || port == -1 || telnetPort == -1) {
        wrong_format(argv[0]);
    }

    radios.timeout = timeout;

    /* client */
    main_sock = createMainSock(port, host_str, &broadcastClient);
    telnet_sock = createTelnetSock(telnetPort);

    spawnTelnetThread(&telnet_sock);


    udp_message_t outMessage;
    client_t outClient;
    outClient.addrLen = sizeof(outClient.addr);

    for(;;) {
        if (currentClient != NULL) {
            udp_message_t* msg = createMessage(MSG_TYPE_KEEPALIVE, NULL, 0);
            sendUdpMessage(main_sock, msg, currentClient);
            freeMessage(msg);
        }

        if (tryRecvUdpMessage(main_sock, &outMessage, &outClient)) {
            switch (outMessage.type) {

                case MSG_TYPE_AUDIO:
                    if (isSameClient(&outClient, currentClient)) {
                        write(1, (char *) outMessage.data, (int) outMessage.dataLen);
                    }
                    break;

                case MSG_TYPE_METADATA:
                    if (isSameClient(&outClient, currentClient) && outMessage.dataLen > 0) {
                        memcpy(currentMeta, outMessage.data, outMessage.dataLen);
                        currentMeta[outMessage.dataLen] = '\0';
                    }
                    break;

                case MSG_TYPE_IAM:
                    lockList(&radios);

                    char* newDescription = malloc(sizeof(char) * (outMessage.dataLen + 1));
                    if (newDescription != NULL) {
                        memcpy(newDescription, outMessage.data, outMessage.dataLen);
                        newDescription[outMessage.dataLen] = '\0';
                    }

                    refreshClient(&radios, &outClient, newDescription);

                    unlockList(&radios);
                    break;
            }

            free(outMessage.messageSelf);
        }

        if (finish)
            break;
    }

    /* Cleanup */
    close(main_sock);

    if (pthread_join(telnet_thread, NULL) < 0) {
        syserr("pthread_join");
    }

    cleanupClientList(&radios);

    return 0;
}