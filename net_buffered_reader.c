#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include "net_buffered_reader.h"
#include "err.h"

// Initial line buffer size
#define LINE_SIZE 1024

buffInfo_t* newBuffInfo(int inDesc) {
    buffInfo_t* info = malloc(sizeof(buffInfo_t));

    if (info == NULL) {
        syserr("malloc");
    }

    info->in_desc = inDesc;
    info->buff_size = LINE_SIZE;
    info->buff_ptr = 0;
    info->scanned_buff_ptr = 0;
    info->buff = malloc(sizeof(char) * LINE_SIZE);
    info->eof = false;

    if (info->buff == NULL) {
        free(info);
        syserr("malloc");
    }

    return info;
}

void freeBuffInfo(buffInfo_t* info) {
    if (close(info->in_desc) < 0)
        syserr("closing stream socket");

    free(info->buff);
    free(info);
}

char* netReadLine(buffInfo_t* info, size_t* outLineLength) {
    for (;;) {
        for (size_t i = info->scanned_buff_ptr; i + 1 < info->buff_ptr; i++) {
            if (info->buff[i] == '\r' && info->buff[i + 1] == '\n') {
                char* ret = malloc(sizeof(char) * (i + 1));

                if (ret == NULL)
                    syserr("malloc");

                memcpy(ret, info->buff, i);
                ret[i] = '\0';

                memmove(info->buff, info->buff + i + 2, info->buff_ptr - i - 2);

                info->scanned_buff_ptr = 0;
                info->buff_ptr -= i + 2;

                *outLineLength = i;
                return ret;
            }
        }
        info->scanned_buff_ptr = info->buff_ptr;

        if (info->buff_size - info->buff_ptr - 1 == 0) {
            char* newBuff = malloc(sizeof(char) * 2 * info->buff_size);
            memcpy(newBuff, info->buff, info->buff_size);
            info->buff_size *= 2;
            free(info->buff);
            info->buff = newBuff;
        }

        ssize_t bytes = read(info->in_desc, info->buff + info->buff_ptr,
                            info->buff_size - info->buff_ptr - 1);

        info->buff_ptr += bytes;

        if (bytes < 0) {
            if (errno == EWOULDBLOCK || errno == EAGAIN) {
                // Timeout happens
                info->eof = true;
                return NULL;
            }
            else {
                // Something strange
                return NULL;
            }
        }

        if (bytes == 0) {
            info->buff[info->buff_ptr] = 0;
            info->eof = true;
            *outLineLength = info->buff_ptr;

            return info->buff;
        }
    }
}

bool netReadChars(buffInfo_t* info, size_t count, char* targetBuff) {

    while (count > info->buff_ptr - info->scanned_buff_ptr) {
        if (info->buff_size - info->buff_ptr - 1 == 0) {
            char* newBuff = malloc(sizeof(char) * 2 * info->buff_size);
            memcpy(newBuff, info->buff, info->buff_size);
            info->buff_size *= 2;
            free(info->buff);
            info->buff = newBuff;
        }

        ssize_t bytes = read(info->in_desc, info->buff + info->buff_ptr,
                            info->buff_size - info->buff_ptr - 1);

        info->buff_ptr += bytes;

        if (bytes < 0) {
            if (errno == EWOULDBLOCK || errno == EAGAIN) {
                // Timeout happens
                info->eof = true;
                return false;
            }
            else {
                // Something strange
                return false;
            }
        }

        if (bytes == 0) {
            info->buff[info->buff_ptr] = 0;
            info->eof = true;
            return false;
        }
    }

    info->scanned_buff_ptr += count;

    memcpy(targetBuff, info->buff, count);
    memmove(info->buff, info->buff + count, info->buff_ptr - count);

    info->scanned_buff_ptr = 0;
    info->buff_ptr -= count;

    return true;
}
