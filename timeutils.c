#include <sys/time.h>
#include <stdlib.h>
#include "timeutils.h"

void setCurrentTimestamp(struct timeval* time) {
    gettimeofday (time, NULL);
}

bool isTimedOut(struct timeval* time, int seconds) {
    struct timeval now;
    setCurrentTimestamp(&now);

    return (now.tv_sec - time->tv_sec) > seconds || (now.tv_sec < time->tv_sec);
}