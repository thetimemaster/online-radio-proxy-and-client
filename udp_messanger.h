#ifndef RADIO_UDP_MESSANGER_H
#define RADIO_UDP_MESSANGER_H

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <netinet/in.h>

/* This stucture represents another peer
 * program using the messanger */
typedef struct client {
    struct sockaddr_in addr;
    socklen_t addrLen;
} client_t;

/* Returns true, if a and b point to structures
 * symbolising same client */
bool isSameClient(client_t *a, client_t *b);

/* Frees client */
void freeClient(client_t* client);

/* Creates copy of given client */
client_t* copyClient(client_t* client);

/* Structure symbolising one message in
 * our messanger system */
typedef struct udp_message {
    short type;
    short dataLen;
    void* data;
    void* messageSelf;
} udp_message_t;

/* Message constructor, message data is copied pointed to existing data */
udp_message_t* createMessage(short messageType, void* data, short dataLength);

/* Frees message and its data */
void freeMessage(udp_message_t* message);

/* Sends message to given client */
void sendUdpMessage(int sock, udp_message_t *message, client_t *to);

/* Tries to receive next message from sock, supports timeout, fills client structure */
bool tryRecvUdpMessage(int sock, udp_message_t *message, client_t *outFrom);

/* Declared message types */
#define MSG_TYPE_DISCOVER 1
#define MSG_TYPE_IAM 2
#define MSG_TYPE_KEEPALIVE 3

#define MSG_TYPE_AUDIO 4
#define MSG_TYPE_METADATA 6

#endif //RADIO_UDP_MESSANGER_H
