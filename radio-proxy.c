#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <signal.h>
#include <arpa/inet.h>
#include <assert.h>

#include "err.h"
#include "net_buffered_reader.h"
#include "udp_messanger.h"
#include "udp_subscription_server.h"
#include "strutils.h"

char* YES = "yes";
char* NO = "no";
char* META_HEADER = "icy-metaint:";
char* NAME_HEADER = "icy-name:";
char* SAMPLE_RADIONAME = "Noname radio";

bool finish;
client_list_t currentClients;
pthread_t listener_thread;

void wrong_format(char* filename) {
    error(true, "Usage: %s -h host -r resource -p port [-m yes|no] [-t timeout] [-P proxyPort [-B broadcastAddr] [-P proxyPort] ]", filename);
}

static void catch_int (int sig) {
    assert(sig == SIGINT);
    finish = true;
}

int createMainSocket(char* host_str, char* port_str, int timeout) {
    int rc;
    int sock;
    struct addrinfo addr_hints, *addr_result;

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock < 0) {
        syserr("Could not connect to socket");
    }

    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_flags = 0;
    addr_hints.ai_family = AF_INET;
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;

    struct timeval tv;
    tv.tv_sec = timeout;
    tv.tv_usec = 0;
    if (setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        syserr("set timout");
    }

    rc =  getaddrinfo(host_str, port_str, &addr_hints, &addr_result);
    if (rc != 0) {
        syserr("getaddrinfo: %s", gai_strerror(rc));
    }

    if (connect(sock, addr_result->ai_addr, addr_result->ai_addrlen) != 0) {
        syserr("Could not connect to host");
    }
    freeaddrinfo(addr_result);

    return sock;
}

int createListenerSocket(short port, char* multicast_addr) {
    // create what looks like an ordinary UDP socket
    int fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0) {
        syserr("socket");
        return 1;
    }

    // allow multiple sockets to use the same PORT number
    u_int yes = 1;
    if (setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char*) &yes, sizeof(yes)) < 0){
        perror("Reusing ADDR failed");
        return 1;
    }

    // set up destination address
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY); // differs from sender
    addr.sin_port = htons(port);

    struct timeval tv;
    tv.tv_sec = 0;
    tv.tv_usec = 100000;
    if (setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
        syserr("set timout");
    }

    // bind to receive address
    if (bind(fd, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
        perror("bind");
        return 1;
    }

    if (multicast_addr != NULL) {
        // use setsockopt() to request that the kernel join a multicast group
        struct ip_mreq mreq;
        mreq.imr_multiaddr.s_addr = inet_addr(multicast_addr);
        mreq.imr_interface.s_addr = htonl(INADDR_ANY);
        if (setsockopt(fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *) &mreq, sizeof(mreq)) < 0) {
            perror("setsockopt");
            return 1;
        }
    }

    return fd;
}

/* This thread will listen for DISCOVER and KEEPALIVE messages */
void* listener(void* arg) {

    client_list_t* list = arg;
    udp_message_t message; /* we will use this to get stuff */
    client_t client; /* we will use it to get stuff */
    client.addrLen = sizeof(client.addr);

    if (pthread_mutex_init(&list->mutex, NULL) < 0) {
        syserr("mutex_init");
    }

    for(;;) {
        if (finish) break;
        if (tryRecvUdpMessage(list->sock, &message, &client)) {
            lockList(list);

            // Someone asks about me
            if (message.type == MSG_TYPE_DISCOVER) {
                sendUdpMessage(list->sock, list->aboutMe, &client);
                if (!refreshClient(list, &client, NULL)) {
                    error(false, "Could not accept/refresh client\n");
                }
            }

            // Someone starts / continues the subscription
            if (message.type == MSG_TYPE_KEEPALIVE) {
                if (!refreshClient(list, &client, NULL)) {
                    error(false, "Could not accept/refresh client\n");
                }
            }

            free(message.messageSelf);

            unlockList(list);
        }
    }

    return NULL;
}

void spawnProxyThread(char* client_port_str, char* broadcast_addr_str, int clientTimeout, char* desc) {
    memset(&currentClients, 0, sizeof(currentClients));

    currentClients.timeout = clientTimeout;
    currentClients.aboutMe = createMessage(MSG_TYPE_IAM, desc, strlen(desc));
    currentClients.sock = createListenerSocket(atoi(client_port_str), broadcast_addr_str);

    if (pthread_create(&listener_thread, NULL, &listener, &currentClients) < 0) {
        syserr("pthread_create");
    }
}

int main (int argc, char *argv[]) {
    /* Sigint catcher */
    struct sigaction action;
    sigset_t block_mask;

    sigemptyset (&block_mask);
    action.sa_handler = catch_int;
    action.sa_mask = block_mask;
    action.sa_flags = SA_RESTART;

    if (sigaction (SIGINT, &action, 0) == -1)
        syserr("sigaction");

    /* Argument validation and parsing */
    char* port_str = NULL;
    char* host_str = NULL;
    char* resource_str = NULL;
    bool order_metadata = false;
    int timeout = 5;

    bool isProxy = false;
    int clientTimeout = 5;
    char* client_port_str = NULL;
    char* broadcast_addr_str = NULL;

    int opt;
    while ((opt = getopt(argc, argv, "h:r:p:m:t:P:B:T:")) != -1) {
        switch (opt) {
            case 'h':
                host_str = optarg;
                break;

            case  'r':
                resource_str = optarg;
                break;

            case 'p':
                port_str = optarg;
                break;

            case 'm':
                if (is_same(optarg, YES))
                    order_metadata = true;

                else if (is_same(optarg, NO))
                    order_metadata = false;

                else
                    wrong_format(argv[0]);
                break;

            case 't':
                timeout = atoi(optarg);
                break;

            case 'P':
                isProxy = true;
                client_port_str = optarg;
                break;

            case 'B':
                isProxy = true;
                broadcast_addr_str = optarg;
                break;

            case 'T':
                isProxy = true;
                clientTimeout = atoi(optarg);
                break;

            default: /* '?' */
               wrong_format(argv[0]);
        }
    }

    if (port_str == NULL || resource_str == NULL || host_str == NULL) {
        wrong_format(argv[0]);
    }

    if (isProxy && (client_port_str == NULL)) {
        wrong_format(argv[0]);
    }

    /* Server connection */
    int sock = createMainSocket(host_str, port_str, timeout);

    /* Request generation */
    const int initialBuffSize = 32;

    char* buff = malloc(sizeof(char) * initialBuffSize);
    if (buff == NULL)
        syserr("malloc");

    size_t buff_size = initialBuffSize;
    size_t end = 0;

    append_sprintf(&buff, &buff_size, &end, "GET %s HTTP/1.0\r\n", resource_str);
    append_sprintf(&buff, &buff_size, &end, "Host: %s:%s\r\n", host_str, port_str);
    append_sprintf(&buff, &buff_size, &end, "Icy-MetaData: %d\r\n", order_metadata);
    append_sprintf(&buff, &buff_size, &end, "User-Agent: MPlayer 1.3.0 (Debian), built with gcc-7\r\n");
    append_sprintf(&buff, &buff_size, &end, "Connection: close\r\n");
    append_sprintf(&buff, &buff_size, &end, "\r\n");

    if (write(sock, buff, end) != (int)end)
        error(false, "Request was not fully written");

    free(buff);

    buffInfo_t* inBuff = newBuffInfo(sock);

    int data_batch = 1024;
    int max_pack_size = 1024;
    bool useMeta = false;
    char* desc = SAMPLE_RADIONAME;

    /* Parse header */
    for (;;) {
        size_t outLen;
        char* line = netReadLine(inBuff, &outLen);

        if (line == NULL) {
            /* Timeout */
            if (inBuff->eof) {
                error(true, "Server stopped sending packages");
            }

            finish = true;
            error(true, "Unknown error while parsing headers");
        }

        if (outLen >= strlen(META_HEADER) && memcmp(line, META_HEADER, strlen(META_HEADER)) == 0) {
            if (!order_metadata) {
                error(true, "Server sends unordered metadata");
            }
            data_batch = str2int(line + strlen(META_HEADER), outLen - strlen(META_HEADER));
            useMeta = true;
        }

        if (outLen >= strlen(NAME_HEADER) && memcmp(line, NAME_HEADER, strlen(NAME_HEADER)) == 0) {
            desc = malloc(outLen - strlen(NAME_HEADER) + 1);
            if (desc == NULL) {
                desc = SAMPLE_RADIONAME;
            } else {
                memcpy(desc, line + strlen(NAME_HEADER), outLen - strlen(NAME_HEADER));
                desc[outLen - strlen(NAME_HEADER)] = '\0';
            }
        }

        free(line);

        if (outLen == 0)
            break;
    }

    /* Starting the listener */
    if (isProxy) {
        spawnProxyThread(client_port_str, broadcast_addr_str, clientTimeout, desc);
    }

    /* Recieve content until sigint */
    for (;;) {

        int left = data_batch;
        while (left > 0) {

            int next_batch = max_pack_size;
            if (next_batch > left) next_batch = left;

            char *buff = malloc(sizeof(char) * next_batch);

            /* Next data batch */
            if (!netReadChars(inBuff, next_batch, buff)) {
                /* Timeout */
                if (inBuff->eof) {
                    error(false, "Server stopped sending packages");
                    free(buff);
                    break;
                }

                finish = true;
                free(buff);
                break;
            }

            /* If we are proxy send it */
            if (isProxy) {
                udp_message_t *dataMessage = createMessage(MSG_TYPE_AUDIO, buff, next_batch);
                if (dataMessage == NULL) {
                    error(false, "Could not create message\n");
                } else {
                    broadcastMessage(dataMessage, &currentClients);
                    freeMessage(dataMessage);
                }
            }

            /* Else output to stdout */
            else {
                write(1, buff, data_batch);
            }

            left -= next_batch;

            free(buff);
        }

        /* We have metadata */
        if (useMeta) {
            char meta_len = 0;
            netReadChars(inBuff, 1, &meta_len);
            int true_len = 16 * meta_len;

            if (true_len > 0) {
                char *metaBuff = malloc(sizeof(char) * true_len);
                netReadChars(inBuff, true_len, metaBuff);

                /* If we are proxy send it */
                if (isProxy) {
                    udp_message_t *metaMessage = createMessage(MSG_TYPE_METADATA, metaBuff, true_len);
                    if (metaMessage == NULL) {
                        error(false, "Could not create message\n");
                    } else {
                        broadcastMessage(metaMessage, &currentClients);
                        freeMessage(metaMessage);
                    }
                } else {
                    write(2, metaBuff, true_len);
                }

                free(metaBuff);
            }
        }

        if (finish)
            break;
    }


    /* Cleanup */
    if (pthread_join(listener_thread, NULL) < 0) {
        syserr("pthread_join");
    }

    if (close(currentClients.sock) < 0) {
        syserr("close");
    }

    cleanupClientList(&currentClients);

    freeBuffInfo(inBuff);

    if (desc != SAMPLE_RADIONAME)
        free(desc);

    return 0;
}