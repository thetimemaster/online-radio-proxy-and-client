#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "strutils.h"
#include "err.h"

void append_sprintf(char** buffPtr, size_t* sizePtr, size_t* endPtr, const char* format, ...) {
    va_list args;

    for (;;) {
        char* buff = *buffPtr;
        va_start(args, format);
        size_t outputSize = vsnprintf(buff + *endPtr, *sizePtr - *endPtr, format, args);
        va_end(args);
        if (outputSize >= *sizePtr - *endPtr - 1) {
            char *newBuff = malloc(sizeof(char) * (*sizePtr) * 2);
            if (newBuff == NULL)
                syserr("malloc");

            memcpy(newBuff, *buffPtr, *sizePtr);
            free(*buffPtr);
            *buffPtr = newBuff;
            *sizePtr *= 2;
        } else {
            *endPtr += outputSize;
            break;
        }
    }

}

bool is_same(char* str, char* template) {

    if (strlen(str) != strlen(template)) {
        return false;
    }

    return memcmp(str, template, strlen(template)) == 0;
}

int str2int(const char* str, int len) {
    int i;
    int ret = 0;
    for(i = 0; i < len; ++i)
    {
        ret = ret * 10 + (str[i] - '0');
    }
    return ret;
}
