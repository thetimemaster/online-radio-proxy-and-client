CC = gcc
CFLAGS = -Wall -Wextra -g
LFLAGS = -pthread
TARGETS = radio-proxy radio-client
FILES = net_buffered_reader udp_messanger udp_subscription_server timeutils strutils err

all: $(TARGETS)

.c.o: $(FILES:%=%.h)
	$(CC) $(CFLAGS) $< -c -o $@

clean:
	@rm -f $(FILES:%=%.o) $(TARGETS:%=%.o) $(TARGETS)

radio-client: $(FILES:%=%.o) radio-client.o
	$(CC) $(LFLAGS) $(CFLAGS) $^ -o $@

radio-proxy: $(FILES:%=%.o) radio-proxy.o
	$(CC) $(LFLAGS) $(CFLAGS) $^ -o $@
