#ifndef HTTPCOOKIES_NET_BUFFERED_READER_H
#define HTTPCOOKIES_NET_BUFFERED_READER_H

#include <stdlib.h>
#include <stdbool.h>

// Buffered TCP reader structure
typedef struct buffInfo {
    int in_desc;
    size_t buff_size;
    size_t buff_ptr;
    size_t scanned_buff_ptr;
    char* buff;
    bool eof;
} buffInfo_t;

// Creates new buffered reader from socket id
buffInfo_t* newBuffInfo(int inDesc);

// Frees reader, its child resources and closes connection
void freeBuffInfo(buffInfo_t* info);

// Reads line until (\r\n) char combination or end of input
// or timeout on the read function.
// eof flag is set to true in the second case
char* netReadLine(buffInfo_t* info, size_t* outLineLength);

// Reads k characters from buffInfo
// Returns false if not enough characters were read before
// the connection was closed, sets eof flag on read timeout
bool netReadChars(buffInfo_t* info, size_t count, char* buffer);

#endif //HTTPCOOKIES_NET_BUFFERED_READER_H
