#ifndef RADIO_TIMEUTILS_H
#define RADIO_TIMEUTILS_H

#include <stdbool.h>
#include <sys/time.h>

/* Sets current time in timeval struct pointed by time */
void setCurrentTimestamp(struct timeval* time);

/* Returns true, if timstamp in time is at least [seconds]s old */
bool isTimedOut(struct timeval* time, int seconds);

#endif //RADIO_TIMEUTILS_H
