#ifndef _ERR_
#define _ERR_

#include <stdbool.h>

/* Displays information about error in syscall and ends
 * the program with exitcode 1 */
extern void syserr(const char *fmt, ...);

/* Displays information about program error and, depending
 * on its severity, continues or ends the program*/
extern void error(bool isFatal, const char *fmt, ...);

#endif
