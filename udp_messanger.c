#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>

#include "udp_messanger.h"
#include "err.h"

#define __BUFF_LEN 40000
static char buffer[__BUFF_LEN];

udp_message_t* createMessage(short messageType, void* data, short dataLength) {
    udp_message_t* message = malloc(sizeof(udp_message_t));

    if (message == NULL) {
        return NULL;
    }

    message->type = messageType;
    message->dataLen = dataLength;

    message->messageSelf = malloc(4 + dataLength);

    if (message->messageSelf == NULL) {
        free(message);
        return NULL;
    }

    message->data = message->messageSelf + 4;

    if (dataLength > 0) {

        memcpy(message->data, data, dataLength);

    }

    ((short*)message->messageSelf)[0] = htons(messageType);
    ((short*)message->messageSelf)[1] = htons(dataLength);

    return message;
}

void freeMessage(udp_message_t* message) {
    free(message->messageSelf);
    free(message);
}

client_t* copyClient(client_t* client) {
    client_t* copy = malloc(sizeof(client_t));
    if (copy == NULL) {
        return NULL;
    }

    memcpy(copy, client, sizeof(client_t));
    return copy;
}

bool isSameClient(client_t *a, client_t *b) {
    if (a == NULL || b == NULL) return false;
    if (a->addrLen != b->addrLen) return false;
    return memcmp(&a->addr, &b->addr, a->addrLen) == 0;
}

void freeClient(client_t* client) {
    free(client);
}

void sendUdpMessage(int sock, udp_message_t *message, client_t *to) {
    if (sendto(sock, message->messageSelf, message->dataLen + 4, 0, (struct sockaddr*)&to->addr, to->addrLen) < 0) {
        syserr("sendto");
    }
}


bool tryRecvUdpMessage(int sock, udp_message_t *message, client_t* outFrom) {

    bool didRecvHead = false;
    int bytes = 0;
    while (true) {
        int r = recvfrom(sock, buffer, __BUFF_LEN, 0, (struct sockaddr *) &outFrom->addr, &outFrom->addrLen);

        if (r < 0) {
            /* We are empty */
            if (errno == EWOULDBLOCK || errno == EAGAIN) {
                return false;
            }
            syserr("recvfrom");
        }

        bytes += r;

        if (bytes >= 4 && !didRecvHead) {
            didRecvHead = true;
            message->type = ntohs(((short*)buffer)[0]);
            message->dataLen = ntohs(((short*)buffer)[1]);
        }

        if (didRecvHead && bytes >= 4 + message->dataLen) {
            message->messageSelf = malloc(4 + message->dataLen);
            if (message->messageSelf == NULL) {
                syserr("malloc");
            }

            memcpy(message->messageSelf, buffer, 4 + message->dataLen);
            message->data = message->messageSelf + 4;

            return true;
        }
    }
}