#ifndef RADIO_UDP_SUBSCRIPTION_SERVER_H
#define RADIO_UDP_SUBSCRIPTION_SERVER_H

#include <pthread.h>
#include "udp_messanger.h"

/* Structure containing info about one client in list */
typedef struct client_list_node {
    struct client_list_node* next_client;
    client_t* peer;
    char* description;
    struct timeval time;
} client_list_node_t;

/* Structure containing info about subscribed clients */
typedef struct client_list {
    client_list_node_t* head;
    client_list_node_t* tail;
    int timeout;
    pthread_mutex_t mutex;
    udp_message_t* aboutMe;
    int sock;
} client_list_t;

/* Locks the mutex on the list */
void lockList(client_list_t* list);

/* Unlcks the mutex on the list */
void unlockList(client_list_t* list);

/* Frees resources in list but not the list */
void cleanupClientList(client_list_t* list);

/* Add / refresh the subsctiption time for given client */
bool refreshClient(client_list_t* list, client_t* client, char* newDesc);

/* Send a message to all clients in a list */
void broadcastMessage(udp_message_t* message, client_list_t* list);

/* Updates the about me messsage */
void updateAboutMe(client_list_t* list, udp_message_t* message);

/* Remove all inactive clients from the list */
void timeoutClients(client_list_t* list);

#endif //RADIO_UDP_SUBSCRIPTION_SERVER_H
