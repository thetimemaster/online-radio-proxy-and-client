#ifndef RADIO_STRUTILS_H
#define RADIO_STRUTILS_H

#include <stdlib.h>
#include <stdbool.h>

/* Appends text to given string buffer, will expand the buffer if needed */
void append_sprintf(char** buffPtr, size_t* sizePtr, size_t* endPtr, const char* format, ...);

/* Check if two strings are equal */
bool is_same(char* str, char* template);

/* Parses string of given length to int */
int str2int(const char* str, int len);

#endif //RADIO_STRUTILS_H
